## MapsEApp

Esta aplicación ha sido desarrollada por Beatriz González tomando como base codigo en link (https://github.com/thecodepath/android_guides/wiki/Google-Maps-Fragment-Guide),
las herramientas que se utilizaron son las siguientes:

1. Java, Android nativo.
2. XML (Diseño de layout).
3. Android Studio.
4. Gestor de base de datos SQLite.

Descripción de aplicación: App ayuda a probar si se tiene los servicios de reproducción estan configurados correctamente en emulador.